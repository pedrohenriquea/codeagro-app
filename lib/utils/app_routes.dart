class AppRoutes {
  static const HOME = '/';
  static const PRINCIPAL = '/principal';
  static const PRODUCT_DETAIL = '/product-detail';
  static const CART = '/cart';
  static const ORDERS = '/orders';
  static const MENU = '/menu';
  static const PRODUCTS = '/products';
  static const PRODUCT_FORM = '/product-form';
  static const LOGIN_ANIMATION = '/login-animation';
}