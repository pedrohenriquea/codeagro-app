import 'package:flutter/material.dart';
import 'package:shop/utils/app_routes.dart';
import 'package:shop/widgets/menu_item.dart';

class MenuScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView(
      padding: const EdgeInsets.all(15),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 200,
        childAspectRatio: 3 / 2,
        crossAxisSpacing: 20,
        mainAxisSpacing: 20,
      ),
      children: [
        MenuItem('Cultura', Color.fromRGBO(223, 254, 0, 1)),
        MenuItem('Cultivo', Color.fromRGBO(223, 254, 0, 1)),
        MenuItem('Lote', Color.fromRGBO(223, 254, 0, 1)),
        MenuItem('Meus Lotes', Color.fromRGBO(223, 254, 0, 1)),
      ],
    );
  }
}
